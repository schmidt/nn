package nn

/**
 */
object MatrixOperations {
  type Matrix = List[List[Double]]

  def dot(matrix1: Matrix, matrix2: Matrix): Matrix = {
    require(!matrix1.isEmpty && !matrix2.isEmpty)
    require(matrix1.head.size == matrix2.size)
    require(!matrix1.exists(_.size != matrix1.head.size))
    require(!matrix2.exists(_.size != matrix2.head.size))
    matrix1 map {
      row =>
        matrix2.transpose map {column => (row zip column map {case (r, c) => r * c}).sum}
    }
  }

  def arith(matrix1: Matrix, matrix2: Matrix)(op: ((Double, Double) => Double)) = {
    require(!matrix1.isEmpty && !matrix2.isEmpty)
    require(matrix1.size == matrix2.size)
    require(matrix1.head.size == matrix2.head.size)
    require(!matrix1.exists(_.size != matrix1.head.size))
    require(!matrix2.exists(_.size != matrix2.head.size))
    matrix1 zip matrix2 map {case (row1, row2) => row1 zip row2 map {case (r1, r2) => op(r1, r2)}}
  }

  def plus(matrix1: Matrix, matrix2: Matrix) = arith(matrix1, matrix2)((a, b) => a + b)

  def minus(matrix1: Matrix, matrix2: Matrix) = arith(matrix1, matrix2)((a, b) => a - b)

  def multiply(matrix1: Matrix, matrix2: Matrix) = arith(matrix1, matrix2)((a, b) => a * b)

  def applyFunction(matrix: Matrix)(func: Double => Double) = matrix map {_ map func}

  def scalarMultiply(scalar: Double, matrix: Matrix) = applyFunction(matrix)(x => x * scalar)

  def appendBottom(matrix1: Matrix, matrix2: Matrix) = matrix1 ++ matrix2

  def appendRight(matrix1: Matrix, matrix2: Matrix) = (matrix1.transpose ++ matrix2.transpose).transpose

  def removeBottomRow(matrix: Matrix) = matrix.dropRight(1)
}
