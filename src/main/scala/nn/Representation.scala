package nn

object NGram {
  def apply(name: String) = new NGram(name)
}

class NGram(val name: String,
            var next: Set[NGram] = Set())

object Representation {

  def main(args: Array[String]) {
    val n = args(0).toInt
    val sq = args(1)

    //val sq = "ACGTACGTACG"
    //val n = 3

    println(s"sequence length: ${sq.size}")
    val ngc = sq.sliding(n, 1).toVector.groupBy(identity).mapValues(_.size)
    println(s"${ngc.size} unique $n-grams.")

    val graph = (sq.sliding(n, 1) map {NGram(_)}).toList
    for (i <- graph; j <- graph; if i != j && j.name.startsWith(i.name.tail)) {
      // wire up graph
      i.next += j
    }

    var c = Set.empty[String]

    def visit(ngram: NGram, path: List[NGram]): Set[String] = {
      if (path.size == graph.size - 1) {
        val result = (ngram.name.toCharArray.toList.reverse ::: (path map {_.name(0)})).reverse.mkString
        c = c + result
        println(c.size)
        return Set(result)
      }

      (for {
        i <- ngram.next
        if !path.contains(i)
      } yield visit(i, ngram :: path)).flatten
    }

    val sequences = (graph map {visit(_, List())}).toSet.flatten
    println(s"Number of sequences: ${sequences.size}")

    /*def search(currentNgram: String, ngramCounts: Map[String, Int]): Iterable[String] = {
      val nGramsLeft = ngramCounts filter {case (k, v) => v != 0}

      if (nGramsLeft.isEmpty) return Vector(currentNgram)

      def overlaps(s1: String, s2: String) = s2.startsWith(s1.tail)
      val overlappers = nGramsLeft.keys filter {currentNgram.isEmpty || overlaps(currentNgram, _)}

      if (overlappers.isEmpty) return Vector()

      def decrease(ngram: String, ngramCounts: Map[String, Int]) = ngramCounts + (ngram -> (ngramCounts(ngram) - 1))
      val sequences = overlappers map {ngram => search(ngram, decrease(ngram, ngramCounts))} filter {!_.isEmpty}

      if (currentNgram.isEmpty) sequences.flatten else sequences.flatten map {currentNgram(0) + _}
    }

    val result = search("", ngc)

    println(result)
    println(s"number of sequences matching $n-gram vector: ${result.size}")*/
  }
}
