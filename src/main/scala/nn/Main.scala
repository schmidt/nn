package nn


/**
  */
object Main {

  // Test weights generated with collection.immutable.Vector.fill(13){util.Random.nextDouble - 0.5}
  val TW = Vector(
    0.48079968627141956, 0.18095873522897288, 0.08386562210379311, -0.16109757963860905,
    -0.059383393636207704, -0.2380083945500544, -0.06064323355039647, 0.4629891266279189,
    -0.13514600148768097, -0.10900499814052012, -0.34652770670434707, -0.21261456158428627, -0.11747758203666936
  )

  // Not used at the moment
  val Xs = List(
    0.1843116, 0.2810526, 0.6601072, 0.6972587, 0.8419432, 1.2637227, 1.3797445,
    1.3879029, 1.7470660, 1.9443677, 2.3207543, 2.6629127, 3.0764649, 3.2393385,
    3.6109653, 4.3438201, 4.4087097, 4.8491367, 5.2626721, 5.3025518, 5.3884404,
    5.5047475, 5.6494811, 5.7696938, 5.8053570, 5.9502143, 6.0737295, 6.1760326,
    6.2669698, 6.3955211, 7.0108610, 7.2066732, 7.2997427, 7.6160452, 7.6728635,
    7.7010796, 7.7094737, 7.7526863, 7.9055113, 7.9489401, 8.0180761, 8.0290964,
    8.1747629, 8.4711964, 8.9439266, 8.9587836, 9.1500718, 9.7809016, 9.8399755,
    9.9435325
  )

  // Not used at the moment
  val Ys = List(
    0.1367634, 0.1677337, 0.4587115, 0.5139222, 0.9138687, 1.7377603,
    1.7389969, 1.9854372, 3.0075334, 3.6223990, 5.6111603, 6.9896398,
    9.4856312, 10.4890689, 12.8276416, 18.8161240, 19.5527662, 23.8551464,
    27.6677689, 28.1730385, 28.9355336, 30.4059015, 31.6663921, 33.5212954,
    33.7334284, 35.7199559, 37.1383702, 38.2178381, 39.1574007, 40.8778683,
    48.9404508, 52.0349139, 53.3680099, 58.1185024, 58.8761556, 59.3107084,
    59.3695302, 59.8053466, 62.2990690, 62.9267612, 64.1931675, 64.3875866,
    66.8343428, 71.5240460, 79.4201417, 80.4371904, 83.4857544, 95.7660391,
    96.6541752, 98.7000676
  )

  val learningRate = 1
  val linearOutput = false

  /*def main(args: Array[String]) {
    def sigmoid(in: Double) = 1.0 / (1.0 + math.exp(-in))
    def sigmoidDeriviative(in: Double) = in * (1.0 - in)

    // One input, one output, one layer with three hidden units, and two bias nodes. Network configuration can be
    // Changed arbitrarily by modifying the matrix dimensions.
    val initialWeights = List(
      List(
        List(TW(0), TW(1)),
        List(TW(2), TW(3)),
        List(TW(4), TW(5))
      ),
      List(
        List(TW(6), TW(7), TW(8), TW(9))
      )
    )

    val nn = new NeuralNetwork(sigmoid, sigmoidDeriviative, learningRate = 0.2, linearOutput = true)

    var wi = initialWeights


    for(i <- 1 to 1000; j <- 0 to 20) {
      val x = -1.0 + 0.1 * j
      val input = List(List(x))
      val target = List(List(math.pow(x, 2)))
      wi = nn.train(input, target, wi, bias = true)
    }

    for (i <- 0 to 20) {
      val x = -1.0 + 0.1 * i
      println(s"$x^2 = ${nn.predict(List(List(x)), wi, bias = true).head.head}")
    }


    println(wi)
  }*/

  def main(args: Array[String]) {
    Representation.main(args)
  }
}
