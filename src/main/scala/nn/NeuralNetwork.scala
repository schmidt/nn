package nn

import nn.MatrixOperations._
import scala.annotation.tailrec

/**
  */
class NeuralNetwork(
                     activation: Double => Double,
                     activationDeriviative: Double => Double,
                     learningRate: Double = 1.0,
                     linearOutput: Boolean = false
                     ) {

  def train(input: Matrix, target: Matrix, weights: List[Matrix], bias: Boolean = false): List[Matrix] = {
    def step(input: Matrix, weights: List[Matrix]): (Matrix, List[Matrix]) = {
      if (weights.isEmpty) {
        val error = minus(target, input)
        return (if (linearOutput) error else multiply(applyFunction(input)(activationDeriviative), error), List[Matrix]())
      }
      val inputAdjusted = if (bias) appendBottom(input, List(List(1.0))) else input

      // feed forward
      val sums = dot(weights.head, inputAdjusted)

      val (error, weightsUpstream) = step(
        if (weights.size == 1 && linearOutput) sums else applyFunction(sums)(activation),
        weights.tail
      )

      // back propagation
      val newWeight = plus(weights.head, scalarMultiply(learningRate, dot(error, inputAdjusted.transpose)))
      val adjusted = applyFunction(inputAdjusted)(activationDeriviative)
      val newError = multiply(dot(newWeight.transpose, error), adjusted)
      (if (bias) removeBottomRow(newError) else newError, newWeight :: weightsUpstream)
    }

    val (_, w) = step(input, weights)
    w
  }

  def predict(input: Matrix, weights: List[Matrix], bias: Boolean = false): Matrix = {
    @tailrec
    def step(input: Matrix, weights: List[Matrix]): Matrix = {
      if (weights.isEmpty) {
        return input
      }
      val sums = dot(weights.head, if (bias) appendBottom(input, List(List(1.0))) else input)
      step(if (weights.size == 1 && linearOutput) sums else applyFunction(sums)(activation), weights.tail)
    }
    step(input, weights)
  }
}
