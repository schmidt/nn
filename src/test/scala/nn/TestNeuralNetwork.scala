package nn

import org.junit.Test
import org.junit.Assert._
import nn.MatrixOperations.Matrix

/**
  */
class TestNeuralNetwork {
  @Test
  def testExample() {
    def activation(in: Double) = 1.0 / (1.0 + math.exp(-in))
    def activationDeriviative(in: Double) = in * (1.0 - in)

    val input = List(List(0.35), List(0.9))
    val weights = List(List(List(0.1, 0.8), List(0.4, 0.6)), List(List(0.3, 0.9)))
    val target = List(List(0.5))


    val nn = new NeuralNetwork(activation, activationDeriviative, learningRate = 1.0, linearOutput = false)

    val adjustedWeights = nn.train(input, target, weights)

    assertEquals(
      List(
        List(List(0.09915663266507302, 0.7978313411387592), List(0.3972257317028594, 0.5928661672359241)),
        List(List(0.27232596506128215, 0.8729983630089926))
      ), adjustedWeights)

    assertEquals(List(List(0.6902834929076443)), nn.predict(input, weights))
    assertEquals(List(List(0.6820312027460466)), nn.predict(input, adjustedWeights))
  }

  @Test
  def testExampleWithBias() {
    def activation(in: Double) = 1.0 / (1.0 + math.exp(-in))
    def activationDeriviative(in: Double) = in * (1.0 - in)

    val input = List(List(0.35), List(0.9))
    val target = List(List(0.5))
    val weights = List(List(List(0.1, 0.8, 0.2), List(0.4, 0.6, 0.5)), List(List(0.3, 0.9, 0.7)))

    val nn = new NeuralNetwork(activation, activationDeriviative, learningRate = 1.0, linearOutput = false)

    val adjustedWeights = nn.train(input, target, weights, bias = true)

    assertEquals(
      List(
        List(
          List(0.09913243797544838, 0.7977691262225816, 0.1975212513584239),
          List(0.3974784450260118, 0.5935160014954588, 0.4927955572171765)),
        List(
          List(0.2665323997054399, 0.8645474695117087, 0.6536536606630483)
        )
      )
      , adjustedWeights)

    assertEquals(List(List(0.8327284285844934)), nn.predict(input, weights, bias = true))
    assertEquals(List(List(0.8183230296931969)), nn.predict(input, adjustedWeights, bias = true))
  }

  @Test
  def testXSquared() {
    def activation(in: Double) = 1.0 / (1.0 + math.exp(-in))
    def activationDeriviative(in: Double) = in * (1.0 - in)

    val input = List(List(5.0))
    val target = List(List(25.0))
    val weights = List(
      List(
        List(0.48079968627141956, 0.18095873522897288),
        List(0.08386562210379311, -0.16109757963860905),
        List(-0.059383393636207704, -0.2380083945500544),
        List(-0.06064323355039647, 0.4629891266279189)
      ),
      List(
        List(-0.13514600148768097, -0.10900499814052012, -0.34652770670434707, -0.21261456158428627, -0.11747758203666936)
      )
    )

    val nn = new NeuralNetwork(activation, activationDeriviative, learningRate = 1.0, linearOutput = true)

    val adjustedWeights = nn.train(input, target, weights, bias = true)

    assertEquals(
      List(
        List(
          List(197.19909565313029, 39.52461792860075),
          List(449.3685247655291, 89.69583424904646),
          List(270.4060674109294, 53.85508176636306),
          List(430.8314161646741, 86.64140100627283)
        ),
        List(
          List(23.621078288815703, 14.30488580255366, 9.089896346405997, 13.579380789040464, 25.429949607572752)
        )
      )
      , adjustedWeights)

    assertEquals(List(List(-0.5474271896094209)), nn.predict(input, weights, bias = true))
    assertEquals(List(List(86.02519083438858)), nn.predict(input, adjustedWeights, bias = true))
  }

  @Test
  def testXSquaredRData() {
    def activation(in: Double) = 1.0 / (1.0 + math.exp(-in))
    def activationDeriviative(in: Double) = in * (1.0 - in)

    val input = List(List(5.0))
    val target = List(List(25.0))
    val weights =
      List(
        List(
          List(1.66116, -15.79225),
          List(0.64759, -3.1732),
          List(1.22025, -9.05668)
        ),
        List(
          List(38.30468, 51.31301, 27.87504, -2.80469)
        )
      )

    val nn = new NeuralNetwork(activation, activationDeriviative, learningRate = 0.002, linearOutput = true)

    val adjustedWeights = nn.train(input, target, weights, bias = true)

    def calculateError(w: List[Matrix]) = {

      val y = ((1 to 10) map {
        x: Int =>
          nn.predict(List(List(x.toDouble)), w, bias = true)
      }).flatten.flatten

      val t = (1 to 10) map {
        x: Int => math.pow(x.toDouble, 2)
      }

      ((y zip t) map {
        case (a, b) => math.pow(a - b, 2)
      }).sum / 2
    }

    println(calculateError(weights))
    println(calculateError(adjustedWeights))

    println(nn.predict(input, weights, bias = true))
    println(nn.predict(input, adjustedWeights, bias = true))



    //assertEquals(List(List(25.08288792197589)), nn.predict(input, weights, bias = true))
    //assertEquals(List(List(3)), nn.predict(input, adjustedWeights, bias = true))
  }

}
